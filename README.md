 * Copyright 2012, Klip Art Studio, LLC
 *
 * BROWSER SUPPORT: Chrome,  Safri 5.0.2, IE8, FF 3.6.10
 * Dependent: Greensock GSAP https://greensock.com
 * 
 * DESCRIPTION: (This code was built in as3 then convert to javascript)
 *   slider    : Main Slider Div ID you would like to target
 *   handle	   : Main Slider handle Div ID you would like to target
 *   amount	   : The range the slider should go to This starts as 0 and end in the value you add here
 *   snap      : if set to true will snap to each point of your amount
 *   onComplete: Call a function once it is complete
 *  
 * USAGE:
 *   var canvas2 = $('#slider').get(0);
 *   var handle2 = $('#slider .handle').get(0);
 *   var sliderAmt = 90;
 *   var currentPosition = 0;
 * 
 *   var scrollerPosition = function(val) {
 *       console.log('scroller position', val);
 *   }

 *   var stage1 = new kas.Slider(this, {
 *       slider: canvas2,
 *       handle: handle2,
 *       amount: sliderAmt,
 *       onComplete: scrollerPosition
 *   });
 * 
 * HTML Needed
 *       <div id='slider' class='slider'>
 *           <div class='handle'></div>
 *       </div>