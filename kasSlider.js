/*!
 * KAS_Slider: 1.2
 * http://www.klipartstudio.com/
 *
 * Copyright 2012, Klip Art Studio, LLC
 *
 * BROWSER SUPPORT: Chrome,  Safri 5.0.2, IE8, FF 3.6.10
 * 
 * DESCRIPTION: (This code was built in as3 then convert to javascript)
 *   slider    : Main Slider Div ID you would like to target
 *   handle	   : Main Slider handle Div ID you would like to target
 *   amount	   : The range the slider should go to This starts as 0 and end in the value you add here
 *   snap      : if set to true will snap to each point of your amount
 *   onComplete: Call a function once it is complete
 *  
 * USAGE:
 *   var canvas2 = $('#slider').get(0);
 *   var handle2 = $('#slider .handle').get(0);
 *   var sliderAmt = 90;
 *   var currentPosition = 0;
 * 
 *   var scrollerPosition = function(val) {
 *       console.log('scroller position', val);
 *   }

 *   var stage1 = new kas.Slider(this, {
 *       slider: canvas2,
 *       handle: handle2,
 *       amount: sliderAmt,
 *       onComplete: scrollerPosition
 *   });
 * 
 * HTML Needed
 *       <div id='slider' class='slider'>
 *           <div class='handle'></div>
 *       </div>
 *
 * AUTHOR: Jose Fonseca, josef@klipartstudio.com
 * DATE: 11/11/2011
 */

var kas = {};

kas.Slider = function(scope, p_parameters) {
    this.scope = scope;
    this.p_obj = p_parameters;
    this.canvas = this.p_obj.slider;
    this.handle = this.p_obj.handle; //$('.handle').get(0);
    this.handleWidth = $(this.p_obj.handle).outerWidth();
    this.minAmt = 0;
    this.maxAmt = $(this.canvas).outerWidth();
    this.useAmt = this.p_obj.amount;
    this.target = this.p_obj.onComplete;
    this.incrementAmt = 100 / this.maxAmt;
    //this.log = document.getElementById('pusher');


    if (window.Touch) {
        this.mouse = utils.captureTouch(this.canvas);
        //alert("touchy touchy");
        this.canvas.addEventListener('touchstart', $.proxy(this.onMouseDown, this), false);
        this.canvas.addEventListener('touchend', $.proxy(this.onMouseUp, this), false);
        this.canvas.addEventListener('touchmove', $.proxy(this.onMouseMove, this), false);
    } else {
        this.mouse = utils.captureMouse(this.canvas);
        //alert("no touchy touchy");
        this.canvas.addEventListener('mousemove', $.proxy(this.onMouseMove, this), false);
        this.canvas.addEventListener('mouseup', $.proxy(this.onMouseUp, this), false);
        this.canvas.addEventListener('mousedown', $.proxy(this.onMouseDown, this), false);
    }
}


kas.Slider.prototype.onMouseDown = function(event) {
    //event.preventDefault();
    //this.log.value = 'MOUSEDOWN ' + this.mouse.x;

    // if (!this.mouse.isPressed) {
    //     return;
    // }
    if (window.Touch) {
        var touch = this.mouse.event.touches[0];
        if (touch != undefined) {
            this._x = touch.pageX;
        }

        //this.log.value = "onMouseDown " + touch.pageX;
    } else {
        this._x = this.mouse.event.x;
    }

    //console.log('MOUSEDOWN ' + this.mouse.x);
    this.canvas.style.cursor = "pointer";

    var handleOffsetPosition = this.findPos(this.handle);
    //console.log('handleOffsetPosition[0] ' + handleOffsetPosition[0]);

    if (this._x >= handleOffsetPosition[0] &&
        this._x <= handleOffsetPosition[0] + this.handleWidth) {

        this.dragging = true;
        this.mouseDown = true;

        if (window.Touch) {

            //alert("touchy touchy");
            //document.addEventListener('touchend', $.proxy(this.onMouseDown, this), false);
            document.addEventListener('touchmove', $.proxy(this.onMouseMove, this), false);
        } else {
            //alert("no touchy touchy");
            //document.addEventListener('mouseup', $.proxy(this.onMouseUp, this), false);
            document.addEventListener('mousemove', $.proxy(this.onMouseMove, this), false);
        }

        this.updateHandle();
    } else {
        this.dragging = false;
        this.updateHandle();
    }
}

kas.Slider.prototype.onMouseUp = function(event) {
    /* console.log('onMouseUp'); */
    this.mouseDown = false;
    this.dragging = false;
    if (window.Touch) {
        //event.preventDefault();
        //alert("touchy touchy");
        //document.removeEventListener('touchend', $.proxy(this.onMouseUp, this), false);
        document.removeEventListener('touchmove', $.proxy(this.onMouseMove, this), false);

        //this.log.value = "onMouseUp  ";
    } else {
        //alert("no touchy touchy");
        //document.removeEventListener('mouseup', $.proxy(this.onMouseUp, this), false);
        document.removeEventListener('mousemove', $.proxy(this.onMouseMove, this), false);
    }

}

kas.Slider.prototype.getValue = function() {
    return this.currAmt;
}

kas.Slider.prototype.snap = function(val) {
    var snapAmt = this.maxAmt / this.useAmt;
    var checkAmt = 0;
    for (var i = 1; i < this.useAmt; i++) {
        if (checkAmt > val) {
            if ((checkAmt - (snapAmt / 2)) < val) {

            } else {
                checkAmt = checkAmt - snapAmt;
                if (checkAmt < 0) {
                    checkAmt = 0;
                }
            }
            return checkAmt;
        } else {
            checkAmt = i * snapAmt;
            if (val > this.maxAmt - snapAmt) {
                checkAmt = this.maxAmt;
            } else if (i == (this.useAmt - 1) && (checkAmt - (snapAmt / 2)) > val) {
                checkAmt = checkAmt - snapAmt;
            }
        }
    }
    return checkAmt;
}
kas.Slider.prototype.updateHandle = function() {
    this.canvas.style.cursor = "auto";
    var scrollFixPosition = this.findPos(this.canvas);
    this.offSetScroll = this._x - scrollFixPosition[0];


    /*
    	console.log('this._x   '  + this._x);
    	console.log('this.canvas.offsetLeft   '  + scrollFixPosition[0] + '    -   ' + scrollFixPosition[1]);
    	console.log('this.canvas.offsetParent   '  + this.canvas.offsetParent);
    	console.log('this.offSetScroll   '  + this.offSetScroll);
    	console.log('offSetHandle 0   '  + offSetHandle);
    */
    if (this.p_obj.snap) {
        var snapAmt = this.snap(this.offSetScroll);
        this.offSetScroll = snapAmt;
    }
    var offSetHandle = this.offSetScroll - (this.handleWidth / 2);
    var percent = this.incrementAmt * this.offSetScroll; //this.offSetScroll;
    /*
    	console.log('offSetHandle 1   '  + offSetHandle);
    	console.log('totalAmt    '  + this.offSetScroll);	
    	console.log('this.handleWidth    '  + this.handleWidth);
    */


    var addAmt = this.useAmt / 100;

    var totalAmt = addAmt * percent;

    if (this.p_obj.type == "switch") {
        if (this.offSetScroll >= (this.maxAmt / 2)) {
            offSetHandle = this.maxAmt - this.handleWidth;
        } else {
            offSetHandle = 0;
        }
    }
    /* 	console.log('offSetHandle  2  '  + offSetHandle); */
    if (this.offSetScroll < this.handleWidth) { offSetHandle = 0 }
    if (offSetHandle > (this.maxAmt - this.handleWidth)) { offSetHandle = this.maxAmt - this.handleWidth }
    /* 	console.log('offSetHandle  3  '  + offSetHandle); */
    if (totalAmt < 0) { totalAmt = 0 }
    if (totalAmt > this.useAmt) { totalAmt = this.useAmt }

    /* 	console.log('offSetHandle  4  '  + offSetHandle); */

    if (this.offSetScroll >= this.minAmt && this.offSetScroll <= (this.maxAmt)) {
        if (this.dragging) {
            this.handle.style.left = offSetHandle + 'px';
        } else {
            TweenMax.to(this.handle, .3, {
                css: { left: offSetHandle },
                ease: Sine.easeOut
            });
            // $(this.handle).stop().animate({ left: offSetHandle + 'px' }, {
            //     duration: 500,
            //     easing: 'easeOutQuad'
            // });
        }
    }

    /* 	console.log('offSetHandle  5  '  + offSetHandle);	 */

    /* 	console.log('totalAmt    '  + totalAmt); */
    this.currAmt = totalAmt;
    if (this.p_obj.onComplete) {
        this.p_obj.onComplete(totalAmt);
    }

}
kas.Slider.prototype.findPos = function(obj) {
    var curleft = curtop = 0;
    do {
        curleft += obj.offsetLeft;
        curtop += obj.offsetTop;
    } while (obj = obj.offsetParent);
    return [curleft, curtop];
}
kas.Slider.prototype.update = function(event) {

}
kas.Slider.prototype.onMouseMove = function(event) {

    /*
    	console.log('ONMOUSE UP ' + event);
    	console.log('this.mouse.x ' + this.mouse.x);
    	console.log('this.mouse.y ' + this.mouse.y);
    */
    // console.log('this.mouse.event X' + this.mouse.event.x); 
    // console.log('this.mouse.event Y' + this.mouse.event.y); 

    this.canvas.style.cursor = "pointer";

    if (window.Touch) {
        //event.preventDefault();

        if (this.dragging == true) {
            var touch = this.mouse.event.touches[0];
            if (touch != undefined) {
                this._x = touch.pageX;
            }
            this.updateHandle();
        }
    } else {
        if (this.dragging == true) {
            this._x = this.mouse.event.x;
            this.updateHandle();
        }
    }
}